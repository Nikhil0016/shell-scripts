Ansible Installation on Linux 7.7

I. 	SSH Preparation
II.	SSH Execution
III.	Ansible Installation
IV.	Ansible Configuration

I. SSH Preparation for  (All worker Nodes)

1) Create couple of instances at AWS and Select Red Hat Linux images 7.2 Version
	Namely as Master and Webgroup

2)	Change ec2-user to root user by applying
	sudo -su or sudo -i

3)	Go to root user and obtain root user password
	passwd root
	
4)	Go to cd /etc/ssh and edit at sshd_config file as per below
	(Apply vi sshd_config)

	Uncommnad the # key of PermitRootLogin yes
        (Delete # key)
	Uncommand the # key on PasswordAuthentication yes
	

5)	Restart the sshd service by applying below
	systemctl restart sshd (for all instances such as Master and Webgroup)

6)	Apply visudo commands for all machine
	Add below one at last line

	root	ALL=(ALL)	NOPASSWD: ALL 
	__________________________________________________________________________________________

II.	SSH Execution (Master Only)

1) ssh-keygen 
Now key has generated which need to be shared across all machine for connection without password

2) ssh-copy-id username@workernode Private Ip Address

Apply ssh-copy-id of all as per combination (ssh-copy-id username@172.31.18.172) : Private Ip Address
User name is at Node you obtain password of user

Example: if you are in master, ssh-copy-id root@172.31.18.172

Check whether ssh key is working or not (First time ask for passwor and no prompt for asking password 2nd time onwards)

3) Ensure that, you can get into worker node from master w/o asking Password

Command : ssh root@<Pvt ip address of Wokrer Node>

(root is a user name and each machine communicate each other w/o asking password)

Example: ssh root@172.31.18.172

By applying exit command, switch from Webgroup to Master 
___________________________________________________________________________________________________________
III.	Ansible Installation

At Control Server only (Master)

yum update -y
yum repolist
yum install wget -y
wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ivh epel-release-latest-7.noarch.rpm
yum install ansible -y

Ensure that ansible has installed or not, by applying the following

ansible --version
_________________________________________________________________________________________________________________

IV.	Ansible Configuration (Master)

Go to /etc/ansible folder by using following command
cd /etc/ansible

You get the hosts file by applying
ls -la

Open with vi hosts
Go to last line, create a group as per below
[webgroup]
below add private ip of webgroup (Pvt id of worker node)

List out all hosts details

ansible all --list-hosts

apply below

ansible all -m ping                      |  for ansible hand shake
_______________________________________________________________________________________________

Pratice
Go to "/etc/ansible" folder and open hosts file with "vi hosts" and add few hosts groups and name of the hosts

ansible --list-hosts all	
ansible --list-hosts "*"
ansible --list-hosts loadbalancer
ansible --list-hosts webserver
ansible --list-hosts webserver:loadbalancer
ansible --list-hosts webserver[0]
ansible --list-hosts webserver[1]
ansible --list-hosts \!loadbalancer
(Display all groups other than Load Balancer group)

_______________________________________________________________________________________________


Write a playbook inside the ansible folder

	1)	Filename with extension of yaml
	2)	Yml progrm as follows
	3)	
		  - hosts: all
		    tasks: 
		    - command: hostname

Run Playbook apply below commands
ansible-playbook <Playbook name>

_________________________________________________________________________________________________________________

Install tree


- hosts: all
  become: true
  tasks:
    - name: I am going to install tree
      yum: name=tree state=present

Run Playbook apply below commands
ansible-playbook <Playbook name>

> ansible-playbook <Playbook Name with extension> (yaml or yml)

_________________________________________

Install Tomcat


- hosts: all
  become: true
  tasks:
  - name: I am going to install tomcat
    yum: name=tomcat state=present

________________________________________________________________________________


New Command With Items

- hosts: all
  become: true
  tasks:
  - name: I am going to install lot of packages
    yum: name={{item}} state=present
    with_items:
     - vim
     - gedit
     - nano
     - wget
     - unzip

For Chef, package ['git', 'tomcat', 'vim']
_____________________________________________________________________________________

Schedule a Playbook Using Cron

As an additional option, you can schedule a playbook to run at a specific time using your servers cron command! To accomplish this, log in to your server as root and run the following command:

At Master Node 

crontab -e

This command opens a temporary cron file in your system’s default text editor and then simply add a line like so:

* * * * * /usr/bin/ansible-playbook /etc/ansible/multiplepck.yaml

this will run the/etc/ansible/playbooks/multiplepck.yaml file at every one minutes. using the ansible-playbook command.

____________________________________________________________________________________________________________________
Create 3 instances
One Master RHEL
One Worker Node with RHEL
One Worker Node with Ubuntu 18.04

- name: Multiple OS Install
  hosts: all
  
  tasks:
  - name: Installing Tree Package on Multiple Platform
    package:
      name: tree
      state: present
      update_cache: true
    become: true
________________________________________________________________________________________________

We would like to install Php7.2 only on Ubuntu and not on Rhel 

- name: Run tasks on all hosts
  hosts: all
  
  tasks:
  - name: Installing Php7.2
    apt:
      name: php7.2
      state: present
      update_cache: true
    become: true
    when: ansible_distribution == 'Ubuntu'

Run the playbooking by using
ansible-playbook <filename.yaml or yml>

______________________________________________________________________________________________________________________
We would like to install Php7.2 only on Ubuntu and MySql on Rhel 

- name: Run tasks on all hosts
  hosts: all
  
  tasks:
  - name: Installing Php7.2
    apt:
      name: php7.2
      state: present
      update_cache: true
    become: true
    when: ansible_distribution == 'Ubuntu'
  - name: Installing MySql
    yum:
      name: mysql
      state: present
      update_cache: true
    become: true
    when: ansible_distribution == 'RedHat'

________________________________________________________________________________________________________


- name: Install Mulitple Package
  hosts: all
  
  tasks:
  - name: Installing Php7.2 and mysql
    apt:
      name: 
        - php7.2
        - php7.2-mysql
      state: present
      update_cache: true
    become: true
    when: ansible_distribution == 'Ubuntu'
_________________________________________________________

Below commands indicates whether sql has installed or not
dpkg -l | grep '\(php\|mysql\)'

apt-list, command for displaying list applcation which installed

____________________________________________










